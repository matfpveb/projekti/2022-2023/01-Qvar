# <img src="client/src/assets/logo.png" width="100"/>            Project QVar   


# Opis :

Web aplikacija koja sadrži veliki broj kulinarskih recepata. Korisniku aplikacije je omogućena pretraga recepata po nazivu jela, filtriranje rezultata po nekoj od odabranih kategorija iz filter menija, čuvanje omiljenih i kreiranje novih recepata.


# Korišćene tehnologije:

![](https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white)
![](https://img.shields.io/badge/Express.js-404D59?style=for-the-badge)
![](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white)
![](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)


# :hammer: Instalacija :

Node možete preuzeti na [ovom](https://nodejs.org/en/download/) linku.

MongoDB možete preuzeti na [ovom](https://www.mongodb.com/try/download/community) linku.


# :wrench: Preuzimanje i pokretanje :


### Uputstvo za preuzimanje i pokretanje:

1. U terminalu se pozicionirati u željeni direktorijum
2. Izvršiti klonirnaje repozitorijuma : `$ https://gitlab.com/matfpveb/projekti/2022-2023/01-Qvar.git`
3. Potrbeno je pozicionirati se u `data` folder i pokrenuti komandu:
```
 ./importData.sh
```
4. Pozicionirajte se u `server` folder i pokrenite komandu:
``` 
./serverStart.sh
```
5. Pozicionirajte se u `client` folder i pokrenite komandu:
``` 
./clientStart.sh
```
6. Aplikacija je pokrenuta na adresi http://localhost:4200/ 
<br>

#### Uputsvo za pokretanje ukoliko ne želite da koristite napisane skript fajlove ( obavezno pre ovoga izvršiti import kolekcija baze podataka ) :

1. Pozicionirajte se u `server` folder i pokrenite sledeće komande:
``` 
npm install
npm run nodemon
```
2. Pozicionirajte se u `client` folder i pokrenite sledeće komande:
``` 
npm install
ng serve
```
3. Aplikacija je pokrenuta na adresi http://localhost:4200/ 
<br>

<hr>

# Shema baze podataka : 
<table>
<tr>
<th>Users</th>
<th>Meals</th>
<th>Favorites</th>
</tr>
<tr>
<td>

 | Polje    | Tip    | Opis                       |
 | -------- | ------ | -------------------------- |
 | username | String | korisničko ime             |
 | salt     | String | podaci za čuvanje lozinke  |
 | hash     | String | podaci za čuvanje lozinke  |
 | email    | String | email adresa korisnika     |
 | name     | String | korisnikovo ime            |
 | surname  | String | korisnikovo prezime        |
 | imgUrl   | String | korisnikova profilna slika |

</td>
<td>

 | Polje           | Tip        | Opis                           |
 | --------------- | ---------- | ------------------------------ |
 | idMeal          | String     | jedinstveni identifikator jela |
 | strMeal         | String     | naziv jela                     |
 | strCategory     | String     | kategorija jela                |
 | strArea         | String     | oblast iz koje jelo dolazi     |
 | strInstructions | String     | Instrukcije za pripremu jela   |
 | strMealThumb    | String     | slika jela                     |
 | strYouube       | String     | link ka snimku pripreme jela   |
 | strSource       | String     | link ka interenet "izvoru"     |
 | strIngredients  | [ObjectId] | lista sastojaka                |

</td>
<td>

 | Polje    | Tip      | Opis                               |
 | -------- | -------- | ---------------------------------- |
 | username | String   | korisničko ime                     |
 | recipes  | [String] | lista omiljenih recepata korisnika |
</td>
</tr>
</table>


# :movie_camera: Demo snimak aplikacije :

Demo snimak aplikacije možete pogledati na [ovom](https://www.youtube.com/watch?v=--B0adw0L6M&ab_channel=DaniloMati%C4%87) linku.
<hr>

## Developers

- [Danilo Matić, 78/2018](https://gitlab.com/DaniloMatic99)
- [Ermin Škrijelj, 194/2018](https://gitlab.com/erminskrijelj)
- [Vladimir Jovanović, 96/2019](https://gitlab.com/vladjov00)
