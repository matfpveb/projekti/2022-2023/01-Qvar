const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
global.__uploadDir = path.join(__dirname, 'uploads');
if (!fs.existsSync(__uploadDir)) {
  fs.mkdirSync(__uploadDir);
}


const mealsAPI = require('./components/meals/mealsApi');
const usersAPI = require('./components/user/usersApi');
const favoritesAPI=require('./components/favorites/favoritesApi')
const app = express();

const mongoDBString = 'mongodb://localhost:27017/kuvar';
const mongoDBReplicationString = 'mongodb://localhost:27017,localhost:27018,localhost:27019/kuvar?replicaSet=rs';
mongoose
  .connect(mongoDBString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('Successfully connected to MongoDB!');
  });

app.use(
  express.urlencoded({
    extended: false,
  })
);
app.use(express.json());

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PATCH, PUT, DELETE');

    return res.status(200).json({});
  }

  next();
});

app.use('/api/meals', mealsAPI);
app.use('/api/users', usersAPI);
app.use('/api/favorites', favoritesAPI);
app.use(express.static(__uploadDir));

app.use(function (req, res, next) {
  const error = new Error('Zahtev nije podrzan od servera');
  error.status = 405;

  next(error);
});

app.use(function (error, req, res, next) {
  const statusCode = error.status || 500;
  res.status(statusCode).json({
    message: error.message,
    status: statusCode,
    stack: error.stack,
  });
});

module.exports = app;



