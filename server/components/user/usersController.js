const User = require('./usersModel');
const mongoose = require('mongoose');
const {uploadFile}=require('../upload/uploadController');
module.exports.registerUser = async (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;
    const email = req.body.email;
    const name = req.body.name;
    const surname = req.body.surname;

  
    try {
      if (!username || !password || !email || !name || !surname) {
        const error = new Error('Please provide all data for a new user');
        error.status = 400;
        throw error;
      }
  
      const jwt= await User.registerNewUser(username, password, email, name, surname);
      return res.status(201).json({
        token: jwt,
      });
    } catch (err) {
      next(err);
    }
  };
  

module.exports.loginUser = async (req, res, next) => {
    const username = req.username;
  
    try {
      const jwt = await User.getUserJWTByUsername(username);
      return res.status(201).json({
        token: jwt,
      });
    } catch (err) {
      next(err);
    }
  };
  
  
  module.exports.changeUserInfoData = async (req, res, next) => {
    const username = req.username;
    const name = req.body.name;
    const email = req.body.email;
    const surname = req.body.surname;

  
    try {
      if (!email || !name || !surname) {
        const error = new Error('New data cannot be empty');
        error.status = 400;
        throw error;
      }
  
      const jwt = await User.updateUserData(username, name, email, surname);
      return res.status(201).json({
        token: jwt,
      });
    } catch (err) {
      next(err);
    }
  };
  
  module.exports.changeProfileImage = async (req, res, next) => {
    const username = req.username;
    try {
      await uploadFile(req, res);
      if (req.file == undefined) {
        const error = new Error('Please upload a file!');
        error.status = 400;
        throw error;
      }
  
      const imgUrl = req.file.filename;
      await User.changeUserProfileImage(username, imgUrl);
  
      const jwt = await User.getUserJWTByUsername(username);
      return res.status(200).json({
        token: jwt,
      });
    } catch (err) {
      next(err);
    }
  };
  