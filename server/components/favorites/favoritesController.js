const Favorites = require('./favoritesModel');
const mongoose = require('mongoose');

const getFavorites =  async function (req, res, next){
    try{
        const username = req.params.username;
        
        const favorites = await Favorites.getFavorites(username);
        const favoritesList=[];
        for(let f of favorites){
            favoritesList.push(f["name"]);
        }
        if (username == undefined) {
            const error = new Error('Username missing');
            error.status = 404;
            throw error;
          }
          res.status(200).json(favoritesList);
        } catch (err) {
          next(err);
        }      
};

const addToFavorites = async (req, res, next) => {
    try {
        const { username, recipesName } = req.body;

        if(!username || !recipesName ) {
            res.status(400).json();
        } else {
            let newFavorite = await Favorites.addToFavorites(username,recipesName);
            if(newFavorite === null){
                newFavorite = await Favorites.createFavorite(username,recipesName);
            }
            if(newFavorite){
                res.status(200).json(newFavorite);
            }
        }
    } catch (err) {
        next(err);
    }
} 

const removeFromFavorites = async (req, res, next) => {
    try {
        const { username, recipesName } = req.body;
        if(username === undefined) {
            res.status(400).json();
        } 
        let data= await Favorites.removeFromFavorites(username, recipesName);
        if(data === null){
            res.status(404).json();
        }else{
            res.status(200).json(data);
        }
    } catch (err) {
        next(err);
    }
};

module.exports = {
    getFavorites,
    addToFavorites,
    removeFromFavorites    
};
  

