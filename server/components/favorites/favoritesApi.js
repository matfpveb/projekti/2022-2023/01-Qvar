const express = require('express');
const favoritesController = require('./favoritesController');

const router = express.Router();

//GET    /api/meals/{strMeal}      GET MEAL BY NAME.
router.get('/:username', favoritesController.getFavorites);

router.put('/:username', favoritesController.removeFromFavorites);

router.put('/', favoritesController.addToFavorites);



module.exports = router;

