const mongoose = require('mongoose');

const favoritesSchema=new mongoose.Schema({
    username:{
        type:String,
        require:true
    },
    recipes:[
        {
            name:{
                type:String
            }
        }
    ]
});

const Favorites=mongoose.model('favorites',favoritesSchema);

async function getFavorites(_username){
    const user=await Favorites.findOne({username:_username}).exec();
    if(user === null){
        return [];
    }
    return user.recipes;
}

async function removeFromFavorites(username, recipe_name){
    await Favorites.findOneAndUpdate(
        { username: username},
        { $pull: { recipes: {name:recipe_name}}}
    );
    const newFav=await Favorites.findOne({username:username}).exec();
    if(newFav === null){
        return null;
    }
    return newFav.recipes;
}

async function addToFavorites(username,recipe_name){
    await Favorites.findOneAndUpdate(
        { username: username},
        { $push: { recipes: {name:recipe_name}}},
        { userFindAndModify: false}
    );
    const newFav=await Favorites.findOne({username:username}).exec();
    if(newFav === null){
        return null;
    }
    return newFav.recipes;
}

const createFavorite=async(username,recipe_name)=>{
    const newFav= new Favorites({
        username : username,
        recipes:{
            name:recipe_name
        }
    });
    await newFav.save();
    return newFav.recipes;
}

module.exports={
    getFavorites,
    removeFromFavorites,
    createFavorite,
    addToFavorites
};