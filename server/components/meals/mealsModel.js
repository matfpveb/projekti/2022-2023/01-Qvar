const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');


const mealsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    idMeal: {
        type: String,
        require: false
    },
    strMeal: {
        type: String,
        require: true   
    },
    strCategory: {
        type: String,
        require: true
    },
    strArea: {
        type: String,
        require: false
    },
    strInstructions: {
        type:String,
        require:true
    },
    strMealThumb: {
        type: mongoose.Schema.Types.String,
        required: false
      },
    strTags:{
        type:  String,
        require: false
    },
    strYoutube: {
        type:  String,
        require: false
    },
    strIngredients:[
        {
            name:{
                type:String
            },
            measure:{
                type:String
            }
        }
    ],
    strSource:{
        type: String,
        require: false
    },
    strImageSource:{
        type: String,
        require: false
    },
    dateModified:{
        type: String,
        require: false
    },
    strAuthor: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false,
      }
    
});

mealsSchema.plugin(mongoosePaginate);


const Meal = mongoose.model('Meal', mealsSchema);

async function paginateThroughMeals(page = 1, limit = 10) {
    return await Meal.paginate({}, { page, limit, populate: 'strAuthor', sort: 'strMeal', projection: ''});
}
  
async function getMealByName(mealName) {
    var nameRegex = new RegExp(escapeRegExp(mealName), 'i');
    return await Meal.find({strMeal : {$regex: nameRegex}}).exec();
}

function escapeRegExp(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
  }

async function getMealById(idMeal) {
    return await Meal.find({idMeal : idMeal}).exec();
}

async function getMealsByCategory(mealCategory) {
    return await Meal.find({strCategory : {$in:mealCategory}}).exec();
}
async function getMealsByArea(areaName) {
    return await Meal.find({strArea : {$in:areaName}}).exec();
}
async function getMealsByCategAndArea(category, area) {
    return await Meal.find({$and: [{strArea : {$in:area}}, {strCategory : {$in:category}}]}).exec();
}
async function getMealsByListOfNames(mealsName) {
    return await Meal.find({strMeal : {$in:mealsName}}).exec();
}
 
 
async function getRandomMeals() {
    return await Meal.aggregate([{$sample:{size:5}}]).exec();
}

async function addIngredient(strMeal,ingredient){
    await Meal.findOneAndUpdate(
        {strMeal:strMeal},
        { $push: {strIngredients:{
            name:ingredient
        }}},
        {useFindAndModify: false}
    );

    const newMeal=await Meal.findOne({strMeal:strMeal}).exec();
    if(newMeal === null){
        return null;
    }
    return newMeal;
}

async function createMeal(strMeal, strCategory, strArea,strInstructions, strIngredients, strAuthor) {

    const meal = new Meal({
        _id : new mongoose.Types.ObjectId(),
        strMeal : strMeal,
        strCategory :strCategory,
        strArea:strArea,
        strInstructions : strInstructions,
        strIngredients : strIngredients
    });
   
    if (strAuthor) {
        meal.strAuthor = strAuthor;
    }
    meal.dateModified = new Date();
    return await meal.save();
}
  
async function addImg(mealName, strMealThumb) {
    const meal = await getMealByName(mealName);

    meal[0].strMealThumb = strMealThumb;
    await meal[0].save();
  }
  

module.exports = {
    paginateThroughMeals,
    getMealByName,
    createMeal,
    addIngredient,
    getMealsByCategory,
    getRandomMeals,
    getMealsByCategAndArea,
    getMealsByArea,
    getMealById,
    getMealsByListOfNames,
    addImg
  };
  