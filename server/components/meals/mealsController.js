const Meal = require('./mealsModel');
const mongoose = require('mongoose');
const { uploadFile } = require('../upload/uploadController');


module.exports.getAllMeals = async function (req, res, next) {
    const page = req.query.page;
    const limit = req.query.limit;
  
    try {
      const meals = await Meal.paginateThroughMeals(page, limit);
      res.status(200).json(meals);
    } catch (err) {
      next(err);
    }
  };
  

module.exports.getMealByName =  async function (req, res, next){
    try{
        const mealName = req.params.strMeal;
        
        const meal = await Meal.getMealByName(mealName);
        
        if (meal == '') {
            const error = new Error(`Meal with name ${mealName} is not found`);
            error.status = 404;
            throw error;
          }
          res.status(200).json(meal[0]);
        } catch (err) {
          next(err);
        }
      
};

module.exports.getMealById =  async function (req, res, next){
    try{
        const idMeal= req.params.idMeal;
        
        const meal = await Meal.getMealById(idMeal);
        
        if (meal == '') {
            const error = new Error(`Meal with id ${idMeal} is not found`);
            error.status = 404;
            throw error;
          }
          res.status(200).json(meal);
        } catch (err) {
          next(err);
        }
      
};

module.exports.getMealsByCategory =  async function(req, res, next){
    try{
        const body = req.body
        
        if(body == undefined){
            res.status(400).json();
        }
        const meal = await Meal.getMealsByCategory(body['catNames']);
        if (meal.length == 0) {
            const error = new Error(`Meal with that category ${body['catNames']} is not found`);
            error.status = 404;
            throw error;
          }
        res.status(200).json(meal);
    }catch(err){
        next(err);
    }
};

module.exports.searchRecipes = async function(req, res, next) {
  try{
    const searchTerm = req.query.strMeal;
    const meals = await Meal.getMealByName(searchTerm);
    res.status(200).json(meals);

  }catch(err) {
    next(err);
  }
}

module.exports.getMealsByArea =  async function (req, res, next){
    const body = req.body
    try{
        if(body == undefined){
            res.status(400).json();
        }
        const foundMeals = await Meal.getMealsByArea(body['areaNames']);

        if (foundMeals.length == 0) {
            const error = new Error(`Meal with area ${body['areaNames']} is not found`);
            error.status = 404;
            throw error;
          }
        res.status(200).json(foundMeals);
    }catch(err){
        next(err);
    }
};
module.exports.getMealsByCategAndArea =  async function (req, res, next){
    const body = req.body
    const category = body['catNames']
    const area = body['areaNames']

    try{
        if(body == undefined){
            res.status(400).json();
        }
        const foundMeals = await Meal.getMealsByCategAndArea(category, area);

        if (foundMeals.length == 0) {
            const error = new Error(`Meal with area ${body['areaNames']} is not found`);
            error.status = 404;
            throw error;
          }
        res.status(200).json(foundMeals);
    }catch(err){
        next(err);
    }
};
module.exports.getMealsByListOfNames =  async function (req, res, next){
    const body = req.body
    const mealNames = body['mealsNames']
    try{
        if(body == undefined){
            res.status(400).json();
        }
        const foundMeals = await Meal.getMealsByListOfNames(mealNames);

        if (foundMeals.length == 0) {
            const error = new Error(`Meal with names: ${body['areaNames']} is not found`);
            error.status = 404;
            throw error;
          }
        res.status(200).json(foundMeals);
    }catch(err){
        next(err);
    }
};

module.exports.addNewMeal= async function(req,res,next){
    const strAuthor = req.userId;
    try{
        const {strMeal,strCategory,strArea,strInstructions,strIngredients}=req.body;
        if(!strMeal || !strCategory || !strInstructions 
            || !strIngredients || !strArea ){
                res.status(400).json();
        }
        else{
            
            let meal = await Meal.createMeal(strMeal, strCategory, strArea,strInstructions,
              strIngredients,strAuthor);
            
            res.status(201).json(meal);
        }
    }

    catch(err){
        next(err);
    }
};

module.exports.getRandomMeals =async function (req, res, next){
    try{
        const randomMeals = await Meal.getRandomMeals();
        if (randomMeals.length == 0) {
            const error = new Error(`Error fetch random meals`);
            error.status = 404;
            throw error;
          }
        
        res.status(200).json(randomMeals);


    }catch(err){
        next(err);
    }
};

module.exports.addRecipeImage = async function (req, res, next) {
   
    try {
      const mealName = req.params.strMeal;

      await uploadFile(req, res);
  
      if (req.file == undefined) {
        const error = new Error('You should upload a file!');
        error.status = 400;
        throw error;
      }
  
      const imgUrl = req.file.filename;

      await Meal.addImg(mealName, imgUrl);

  
      res.status(200).send({
        message: 'Successfully upoladed file: ' + req.file.originalname,
      });
    } catch (err) {
      next(err);
    }
  };
  
