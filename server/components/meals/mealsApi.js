const express = require('express');
const controller = require('./mealsController');

const router = express.Router();

const authentication = require('../utils/authentication');

//postavljanje ruta

// GET   /api/meals/               GET ALL MEALS.
router.get('/', controller.getAllMeals);

//GET  /api/meals/radnomtest/     GET RANDOM MEALS.
router.get('/random/', controller.getRandomMeals);

router.get('/search/', controller.searchRecipes);

//GET    /api/meals/{strMeal}      GET MEAL BY NAME.
router.get('/:strMeal', controller.getMealByName);

//GET    /api/meals/id/{idMeal}      GET MEAL BY ID.
router.get('/id/:idMeal', controller.getMealById);

//GET  /api/meals/category/{strCategory}     GET MEAL BY CATEGORY.
router.patch('/category/', controller.getMealsByCategory);

//GET  /api/meals/area/{strCountry}     GET MEAL BY AREA.
router.patch('/area/', controller.getMealsByArea);

router.patch('/filter/', controller.getMealsByCategAndArea);

// PATCH MEAL FROM LIST OF NAMES FROM FAVORITES
router.patch('/list/', controller.getMealsByListOfNames);

//POST  /api/meals/                 ADD MEAL
router.post('/', controller.addNewMeal);

router.patch('/upload/:strMeal', controller.addRecipeImage);

module.exports = router;

