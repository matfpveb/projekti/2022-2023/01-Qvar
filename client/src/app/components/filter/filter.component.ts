import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

declare const $: any;


@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit{

  @Output() filterName: EventEmitter<[string, string][]> = new EventEmitter<[string, string][]>();
 
  myFilter: [string, string][] = [];

  filterForm: FormGroup;

  categories: String[] = [
    'Side', 'Lamb', 'Vegan', 'Beef', 'Chicken', 'Vegetarian', 'Pasta',
    'Breakfast', 'Goat', 'Miscellaneous', 'Starter', 'Seafood', 'Pork', 'Dessert'
  ];

  areas: String[] = [
    'Vietnamese', 'Unknown', 'Egyptian', 'Thai', 'Indian', 'Greek', 'Irish',
    'Japanese', 'Jamaican', 'Chinese', 'Turkish', 'Portuguese', 'Dutch', 'French',
    'Spanish', 'Kenyan', 'Tunisian', 'Croatian', 'American', 'Italian', 'Moroccan',
    'Canadian', 'Russian', 'British', 'Malaysian', 'Polish', 'Mexican'
  ];

 

  get categoriesFormArray(): FormArray {
    return this.filterForm.get("categoriesForm") as FormArray;
  }
  get areasFormArray(): FormArray {
    return this.filterForm.get("areasForm") as FormArray;
  }
  get searchFormArray(): FormArray {
    return this.filterForm.get("searchByIngredientsForm") as FormArray;
  }

  constructor(private recipeService: RecipeService){
    this.filterForm = new FormGroup({
      categoriesForm: new FormArray([]),
      areasForm: new FormArray([]),
      searchByIngredientsForm: new FormControl("",[])
    });
    this.addCheckboxes();

  }

  private addCheckboxes() {
    this.categories.forEach(() => this.categoriesFormArray.push(new FormControl(false)));
    this.areas.forEach(() => this.areasFormArray.push(new FormControl(false)));
  }

  onFilter(): void {
    const data = this.filterForm.value;
    const selectedCategories = data.categoriesForm;
    const selectedAreas = data.areasForm;
    const searchIngredients = data.searchByIngredientsForm;
    this.myFilter = [];

    for(let i = 0; i < this.categories.length; i++){
      if(selectedCategories[i]){
        var tmp:[string, string] = ["category", this.categories[i].toString()]
        this.myFilter.push(tmp) 
        }
    }
    
    for(let i = 0; i < this.areas.length; i++){
      if(selectedAreas[i]){
        var tmp:[string, string] = ["area", this.areas[i].toString()]
        this.myFilter.push(tmp)
        }
    }
    const list = searchIngredients.split(",");
    for (const val of list) {
      console.log(val);
    }
    this.filterName.emit(this.myFilter)
  }

  ngOnInit(): void {
    $('.ui.radio.checkbox').checkbox();
  }
}
