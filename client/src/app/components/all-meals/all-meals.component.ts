import { Component, Input, OnInit } from '@angular/core';
import { combineLatest, forkJoin, merge, Observable } from 'rxjs';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-all-meals',
  templateUrl: './all-meals.component.html',
  styleUrls: ['./all-meals.component.css']
})
export class AllMealsComponent implements OnInit{

  recipes: Observable<Recipe[]> | undefined; 
  haveFilter: boolean = false;

  constructor(private recipeService: RecipeService) {

  }


  ngOnInit(): void{

  }
  onGetFilterName(myFilter: [string,string][]){
    this.haveFilter = true;

    var categoryNames: string[] = [];
    var areaNames: string[] = [];

    for (let tuple of myFilter){
      if(tuple[0] == "category"){
        categoryNames.push(tuple[1])
      }else if(tuple[0] == "area"){
        areaNames.push(tuple[1])
      }

    }

    if(categoryNames.length != 0 && areaNames.length == 0){
      this.recipes = this.recipeService.getRecipeByCategory(categoryNames || "");
     
    }else if(areaNames.length != 0 && categoryNames.length == 0){
      this.recipes = this.recipeService.getRecipeByArea(areaNames || "");
    }else if(categoryNames.length != 0 && areaNames.length != 0){
      this.recipes = this.recipeService.getRecipeByCategAndArea(categoryNames, areaNames);
    }
  }
}
