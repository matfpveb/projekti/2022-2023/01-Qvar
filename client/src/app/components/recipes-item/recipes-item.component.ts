import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from '../../models/recipe.model';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription, Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-recipes-item',
  templateUrl: './recipes-item.component.html',
  styleUrls: ['./recipes-item.component.css']
})
export class RecipesItemComponent implements OnInit{
  
  @Input() recipe: Recipe;
  displayHeart:boolean=false;
  public user: User | null = null;
  private userSub: Subscription;
  private isAdded: boolean = false;
  public heart:string="heart outline icon";
  private favSub: Subscription | undefined;

  constructor(private auth: AuthService, private recipeService: RecipeService) {
    const emptyIngr: {name: string, measure: string} = {name: "", measure: ""};
    this.recipe=new Recipe("","","","","",[emptyIngr],"","","",0);
    
    this.userSub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });
    this.auth.sendUserDataIfExists();

    if(this.user != null){
      this.favSub = this.recipeService.getFavorites(this.user!.username).subscribe((list: string[]) => {
          if(list.includes(this.recipe.strMeal)) {
          this.isAdded=true;
          this.heart="heart icon";
        }
      });
    } 
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
    if(this.favSub != undefined) 
      this.favSub.unsubscribe();
  }

  addToFavoritesButtonClicked(){
    if(!this.isAdded) {
      this.isAdded=true;
      this.heart="heart icon";
      this.recipeService.addToFavorites(this.user!.username,this.recipe.strMeal);
    }
    else {
      this.isAdded=false;
      this.heart="heart outline icon";
      this.recipeService.removeFromFavorites(this.user!.username, this.recipe.strMeal);
    }
  }
  getImageUrl(tmp:string): string {
    return 'http://localhost:3000/' + tmp;
  }
  getRecipePicture(tmp: string):string{
    if(tmp === undefined || this.getImageUrl(tmp)==="")  {
      return 'assets/diet.png';
    }
    if(tmp.startsWith("https")){
      return tmp;
    }
    
    return this.getImageUrl(tmp);
  }
}

