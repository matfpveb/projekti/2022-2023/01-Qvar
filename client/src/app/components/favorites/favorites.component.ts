import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subscription, tap } from 'rxjs';
import { Recipe } from 'src/app/models/recipe.model';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit{
  
  
  public user: User | null = null;
  private userSub: Subscription;
  private favSub: Subscription;
  favRecipes: string[] | null = null
  public recipesList: Observable<Recipe[]> = new Observable<Recipe[]>();


  constructor(private auth: AuthService, private recipeService: RecipeService) {
    
    this.userSub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });
    this.auth.sendUserDataIfExists();
    
    
    this.favSub = this.recipeService.getFavorites(this.user!.username).subscribe((list: string[]) => {
       this.recipesList = this.recipeService.getMealsByListOfNames(list)
      });
   
  }
  
  ngOnInit(): void {
  }
  ngOnDestroy(): void {
    this.userSub.unsubscribe();
    this.favSub.unsubscribe();
  }

  removeFromFavoritesButtonClicked(mealName: string){
    this.recipeService.removeFromFavorites(this.user!.username, mealName);
    window.location.reload();
  }
}
