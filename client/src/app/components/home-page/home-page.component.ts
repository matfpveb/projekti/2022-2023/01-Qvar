import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
    @Input() recipes: Observable<Recipe[]> = {} as Observable<Recipe[]>;
  
    constructor(private recipeService: RecipeService) {
      this.recipes = this.recipeService.getRandomRecipes();
    }
  
    ngOnInit(): void{
    }
}
