import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Recipe } from 'src/app/models/recipe.model';
import { AuthService } from 'src/app/services/auth.service';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit,OnDestroy{

  public recipe:  Observable<Recipe> = new Observable<Recipe>();
  private sub: Subscription = new Subscription();
  public mealName: string | null = null;
  private recSub: Subscription = new Subscription();
  public youTubeId: string | null = null;
  
  constructor(private activatedRoute: ActivatedRoute, private recipeService: RecipeService, private auth: AuthService, private router: Router) {
    this.sub = this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      const strMeal: string | null = params.get('strMeal');
      this.mealName = strMeal
      this.recipe = this.recipeService.getRecipeByName(this.mealName!)
    });
    this.auth.sendUserDataIfExists();
    this.recSub = this.recipe.subscribe((rec: Recipe) =>{
      if(rec.strYoutube !== undefined){
        this.youTubeId = rec.strYoutube.split("=")[1];
     }
    });
  } 

  ngOnInit(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.recSub) {
      this.recSub.unsubscribe();
    }
  }
  getImageUrl(tmp:string): string {
    return 'http://localhost:3000/' + tmp;
  }
  getRecipePicture(tmp: string):string{
    if(tmp === undefined || this.getImageUrl(tmp)==="")  {
      return 'assets/diet.png';
    }
    if(tmp.startsWith("https")){
      return tmp;
    }
    
    return this.getImageUrl(tmp);
  }

}
