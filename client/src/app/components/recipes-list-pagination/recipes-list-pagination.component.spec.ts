import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipesListPaginationComponent } from './recipes-list-pagination.component';

describe('RecipesListPaginationComponent', () => {
  let component: RecipesListPaginationComponent;
  let fixture: ComponentFixture<RecipesListPaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecipesListPaginationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RecipesListPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
