import { Component, Input, OnInit } from '@angular/core';
import {RecipePagination} from "../../models/recipe-pagination"
@Component({
  selector: 'app-recipes-list-pagination',
  templateUrl: './recipes-list-pagination.component.html',
  styleUrls: ['./recipes-list-pagination.component.css']
})
export class RecipesListPaginationComponent implements OnInit{
  
  @Input() pagination: RecipePagination | undefined;

  constructor(){}
  
  ngOnInit() {
    
  }

  public getArrayOfPageNumbers(totalPages: number): number[] {
    return [...Array(totalPages).keys()].map((page: number) => page + 1);
  }
}
