import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged, Observable, Subject, switchMap } from 'rxjs';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-recipe-search',
  templateUrl: './recipe-search.component.html',
  styleUrls: ['./recipe-search.component.css']
})
export class RecipeSearchComponent {
  @ViewChild('searchBox') searchBox: ElementRef<HTMLInputElement> | undefined;

  private searchTerms = new Subject<string>();
  recipes$!: Observable<Recipe[]>

  searchForm: FormGroup;
  public strMeal: string = "";

  constructor(private recipeService: RecipeService, private router: Router) {
    this.searchForm = new FormGroup({
      search: new FormControl('',[])
    });
  }

  ngOnInit(): void {
    this.recipes$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => this.recipeService.searchRecipes(term))
    )
  }

  searchList(term: string): void {
    this.searchTerms.next(term);
  }

  clearSearch() {
    this.searchBox!.nativeElement.value = "";
    this.searchList("");
  }
  
  searchOnPress(): void {
    const data = this.searchForm.value;
   
    this.strMeal = data.search;
    this.recipeService.searchRecipes(this.strMeal).subscribe((recepies: Recipe[]) => {
      this.router.navigateByUrl(`/meals/${recepies[0].strMeal}`);
    })
   
    this.clearSearch();
    this.searchForm.reset();
  }

  showSuggestions: boolean = false;

  toggleSearchSuggestions(): void {
    this.showSuggestions = !this.showSuggestions;
  }

  clickedOutside(): void {
    this.showSuggestions = false;
    this.searchList("");
  }
}
