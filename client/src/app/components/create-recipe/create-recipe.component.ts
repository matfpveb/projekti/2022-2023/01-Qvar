import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Recipe } from "../../models/recipe.model";
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { RecipeNameValidator } from 'src/app/validators/recipe-name.validator';
import { RecipeService } from 'src/app/services/recipe.service';
import { Router } from '@angular/router';
import { IngredientsValidator } from 'src/app/validators/ingredients.validators';

declare const $: any;

@Component({
  selector: 'app-create-recipe',
  templateUrl: './create-recipe.component.html',
  styleUrls: ['./create-recipe.component.css']
})
export class CreateRecipeComponent implements OnInit {

  createRecipeForm: FormGroup;
  private image: File | null = null;

  @Output() recipeCreated: EventEmitter<Recipe> = new EventEmitter<Recipe>();

  constructor(private formBuilder: FormBuilder,private recipeService:RecipeService,private router: Router) { 
    this.createRecipeForm = this.formBuilder.group({
      strMeal: ['', [Validators.required, Validators.minLength(3), RecipeNameValidator]],
      strCategory: ['', [Validators.required, Validators.maxLength(200)]],
      strArea: ['', [Validators.required, Validators.maxLength(200)]],
      strInstructions: ['', [Validators.required, Validators.maxLength(2000)]],
      strIngredients: ['', [Validators.required,IngredientsValidator]],
      strMealThumb : ['']
    });
  }

  ngOnInit(): void {
  }

  onCreateRecipeFormSubmit() {
    if (this.createRecipeForm.invalid) {
      window.alert('Some fields do not have valid values!');
      return;
    }

    const data = this.createRecipeForm.value;
  
    const ingredientsString = data.strIngredients;
    const listOfIngredients = ingredientsString.split(";");
    //listOfIngredients : ["name,measue", "name,measure", "name,measure"...]
    var strIngredientsList = []
    
    for (let item of listOfIngredients){
        
        const tmp = item.split(",")
        const name = tmp[0]
        const measue = tmp[1]
        var ingredient :{name: string, measure: string} = {name: "", measure: ""};
        ingredient.name = name
        ingredient.measure = measue

        strIngredientsList.push(ingredient);
    }


    this.recipeService.addNewMeal(data.strMeal, data.strCategory, data.strArea, data.strInstructions,strIngredientsList," ");

    if(this.image !== null){
      this.recipeService.patchMealImage(this.image, data.strMeal).subscribe((message: string)=>{
        console.log(message);
      });
    }

    this.createRecipeForm.reset({strMeal: '', strCategory: '', strArea: '', strInstructions: '', strIngredients:''});
    this.router.navigateByUrl('/meals');
  }
  
  public onFileChange(event: Event): void {
    const files: FileList | null = (event.target as HTMLInputElement).files;
    if(!files || files.length === 0) {
        this.image = null;
        return;
    }
    this.image = files[0];
  }
  nameHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.createRecipeForm.get('strMeal')?.errors;

    return errors !== null;
  }

  instructionHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.createRecipeForm.get('strInstructions')?.errors;

    return errors !== null;
  }


  ingredientsHaveErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.createRecipeForm.get('strIngredients')?.errors;

    return errors !== null;
  }

  nameErrors(): string[] {
    const errors: ValidationErrors | null | undefined = this.createRecipeForm.get('strMeal')?.errors;

    if (errors == null) {
      return [];
    }

    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('Recipe must have a name.');
    }

    if (errors['minlength']) {
      errorMsgs.push(`Recipe name is ${errors['minlength'].actualLength} characters long, when it should be at least ${errors['minlength'].requiredLength} characters.`);
    }

    if (errors['recipeNameValidatorError']) {
      errorMsgs.push(errors['recipeNameValidatorError'].message);
    }

    
    return errorMsgs;
  }

  instructionErrors():string[]{
    const errors: ValidationErrors | null | undefined = this.createRecipeForm.get('strInstructions')?.errors;

    if (errors == null) {
      return [];
    }

    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('Recipe must have an instruction.');
    }

    if (errors['maxlength']) {
      errorMsgs.push(`MaxLenght of instruction is 2000 characters`);
    }
    return errorMsgs;
  }


  ingredientsErrors():string[]{
    const errors: ValidationErrors | null | undefined = this.createRecipeForm.get('strIngredients')?.errors;

    if (errors == null) {
      return [];
    }

    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('Recipe must have minimum one ingredient.');
    }

    if (errors['ingredientsValidatorError']) {
      errorMsgs.push(errors['ingredientsValidatorError'].message);
    }


    return errorMsgs;
  }
}
