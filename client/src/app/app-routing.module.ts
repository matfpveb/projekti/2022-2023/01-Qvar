import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { LogoutComponent } from './components/logout/logout.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { UserAuthenticatedGuard } from './guards/user-authenticated.guard';
import { CreateRecipeComponent } from './components/create-recipe/create-recipe.component';
import { RecipeDetailsComponent } from './components/recipe-details/recipe-details.component';
import { AllMealsComponent } from './components/all-meals/all-meals.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { RecipesListComponent } from './components/recipes-list/recipes-list.component';
import { FavoritesComponent } from './components/favorites/favorites.component';


const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'register', component: RegistrationFormComponent},
  {path: 'login', component: LoginFormComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'user', component: UserProfileComponent, canActivate: [UserAuthenticatedGuard]},
  {path: 'create-recipe', component: CreateRecipeComponent, canActivate: [UserAuthenticatedGuard]},
  {path: 'meals', component: AllMealsComponent},
  {path: 'meals', component: RecipesListComponent},
  {path: 'meals/:strMeal', component: RecipeDetailsComponent},
  {path: 'favorites', component: FavoritesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
