import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { RecipesListComponent } from './components/recipes-list/recipes-list.component';
import { RecipesItemComponent } from './components/recipes-item/recipes-item.component';
import { CreateRecipeComponent } from './components/create-recipe/create-recipe.component';
import { AllMealsComponent } from './components/all-meals/all-meals.component';
import { FilterComponent } from './components/filter/filter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { NavigationMenuComponent } from './components/navigation-menu/navigation-menu.component';
import { HttpClientModule } from '@angular/common/http';
import { RecipeDetailsComponent } from './components/recipe-details/recipe-details.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { RecipesListPaginationComponent } from './components/recipes-list-pagination/recipes-list-pagination.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { RecipeSearchComponent } from './components/recipe-search/recipe-search.component';
import { ClickOutsideDirective } from './directives/clickOutside.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    UserProfileComponent,
    RecipesListComponent,
    RecipesItemComponent,
    CreateRecipeComponent,
    AllMealsComponent,
    FilterComponent,
    LoginFormComponent,
    LogoutComponent,
    RegistrationFormComponent,
    NavigationMenuComponent,
    RecipeDetailsComponent,
    RecipesListPaginationComponent,
    FavoritesComponent,
    RecipeSearchComponent,
    ClickOutsideDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    YouTubePlayerModule,
    NgxImageZoomModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
