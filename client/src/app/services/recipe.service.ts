import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, tap, of } from 'rxjs';
import { RecipePagination,IRecipePagination } from '../models/recipe-pagination';
import { Recipe, IRecipe } from '../models/recipe.model';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  constructor(private http: HttpClient) { }

  public getRecipes(page: number = 1, limit: number = 10): Observable<RecipePagination> {
    const queryParams: HttpParams = new HttpParams().append('page', page).append('limit', limit);
    return this.http.get<IRecipePagination>("http://localhost:3000/api/meals", { params:queryParams, })
          .pipe(map((pagination: IRecipePagination) => this.createPaginateFromPojo(pagination)));
  }
  createPaginateFromPojo(pojo: IRecipePagination): RecipePagination {
    const pagination: RecipePagination = new RecipePagination();
    pagination.docs = pojo.docs.map((doc: IRecipe) => this.createRecipeFromPOJO(doc));
    pagination.hasNextPage = pojo.hasNextPage;
    pagination.hasPrevPage = pojo.hasPrevPage;
    pagination.limit = pojo.limit;
    pagination.nextPage = pojo.nextPage;
    pagination.page = pojo.page;
    pagination.pagingCounter = pojo.pagingCounter;
    pagination.prevPage = pojo.prevPage;
    pagination.totalDocs = pojo.totalDocs;
    pagination.totalPages = pojo.totalPages;
    return pagination
  }
  createRecipeFromPOJO(doc: IRecipe): Recipe {
   return new Recipe(
    doc.idMeal,
    doc.strMeal,
    doc.strCategory,
    doc.strArea,
    doc.strInstructions,
    doc.strIngredients,
    doc.strMealThumb,
    doc.strAuthor,
    doc.strYoutube,
    doc.rating,
   );
  }

  public getRecipeByName(strMeal: string): Observable<Recipe> {
    return this.http.get<Recipe>(`http://localhost:3000/api/meals/${strMeal}`);
  }

  public getRecipeById(idMeal: string): Observable<Recipe> {
    return this.http.get<Recipe>("http://localhost:3000/api/meals/id/"+idMeal);
  }

  public getRandomRecipes(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(`http://localhost:3000/api/meals/random`);
  }


  public addNewMeal(strMeal:string, strCategory:string, strArea:string, strInstructions: string,
          strIngredients: {name: string,measure: string}[], strMealThumb: string):void{
      const body = {"strMeal":strMeal, "strCategory":strCategory,"strArea":strArea,
                    "strInstructions":strInstructions,"strIngredients":strIngredients,
                    "strMealThumb":strMealThumb}
      
      this.http.post<Recipe>(`http://localhost:3000/api/meals/`,body).
      subscribe((data)=>{console.log(data);});

  }
  public patchMealImage(file: File, mealName: string): Observable<string>{
    const body: FormData = new FormData();
    body.append("file", file);

    return this.http.patch<string>(`http://localhost:3000/api/meals/upload/${mealName}`,body);
  }

  public getRecipeByCategory(categoryNames: string[]): Observable<Recipe[]>{
    const body = {"catNames" : categoryNames}
    return this.http.patch<Recipe[]>(`http://localhost:3000/api/meals/category/`,body);
  }


  public getRecipeByArea(areaNames: string[]): Observable<Recipe[]>{
    const body = {"areaNames" : areaNames}
    return this.http.patch<Recipe[]>(`http://localhost:3000/api/meals/area/`,body);
  }

  public getRecipeByCategAndArea(categoryNames: string[], areaNames: string[]): Observable<Recipe[]>{
    const body = {"catNames": categoryNames,
                  "areaNames" : areaNames}
    return this.http.patch<Recipe[]>(`http://localhost:3000/api/meals/filter/`,body);
  }

  public getMealsByListOfNames(mealsNames: string[]): Observable<Recipe[]>{
    const body = {"mealsNames" : mealsNames}
    return this.http.patch<Recipe[]>(`http://localhost:3000/api/meals/list/`,body);
  }

  public addToFavorites(username:string, strMeal:string):void{
    const body={
                "username":username,
                "recipesName":strMeal
                };
    this.http.put<string>(`http://localhost:3000/api/favorites/`,body)
      .subscribe((data) => {
        console.log("Recipe Service - Adding to favorites");
        console.log(data);
      });
  }

  public removeFromFavorites(username: string, strMeal: string): void {
    const body = {
      "username":username,
      "recipesName":strMeal
    };
    this.http.put<string>(`http://localhost:3000/api/favorites/${username}`,body)
      .subscribe((data) => {
        console.log("Recipe Service - Removing from favorites");
        console.log(data);
      });

  }

  public getFavorites(username:string):Observable<string[]>{
    return this.http.get<string[]>(`http://localhost:3000/api/favorites/${username}`);
  }

  public searchRecipes(term: string): Observable<Recipe[]> {
    if(!term.trim()) {
      return of([]);
    }
    return this.http.get<Recipe[]>(`http://localhost:3000/api/meals/search/?strMeal=${term}`);
  }
}
