import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtService } from './jwt.service';
import { AuthService } from './auth.service';
import { Observable, map, tap } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly urls = {
    patchUser: "http://localhost:3000/api/users",
    patchUserProfileImage: "http://localhost:3000/api/users/profile-image"
}

  constructor(private http: HttpClient,
              private jwt: JwtService,
              private auth: AuthService) {}

  public patchUserData(username: string, name: string, surname: string, email: string): Observable<User> {
    const body = {username, name, surname,email};
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);

    return this.http.patch<{token: string}>(this.urls.patchUser, body, {headers}).pipe(
        tap((response: {token: string}) => this.jwt.setToken(response.token)),
        map((response: {token: string}) => this.auth.sendUserDataIfExists()!)
    )
  }

  public patchUserProfileImage(file: File): Observable<User> {
    const body: FormData = new FormData();
    body.append("file", file);
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);

    return this.http.patch<{token: string}>(this.urls.patchUserProfileImage, body, {headers}).pipe(
        tap((response: {token: string}) => this.jwt.setToken(response.token)),
        map((response: {token: string}) => this.auth.sendUserDataIfExists()!)
    )
  }

}
