import { AfterViewChecked,Component, OnInit } from '@angular/core';
import { Recipe } from './models/recipe.model';
import { Observable, Subscription } from 'rxjs';
import { User } from './models/user.model';
import { RecipeService } from './services/recipe.service';
import { AuthService } from './services/auth.service';

declare const $:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewChecked{
  title = 'client';

  recipesFromRoot: Observable<Recipe[]>;
  user: User | null=null;
  sub: Subscription;
 
  constructor(private recipeService: RecipeService, private auth: AuthService){
    this.recipesFromRoot=this.recipeService.getRandomRecipes();
    this.sub=this.auth.user.subscribe((user: User | null)=>{this.user=user;})
  }
  apiLoaded = false;
  ngOnInit():void{
    if (!this.apiLoaded) {
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      document.body.appendChild(tag);
      this.apiLoaded = true;
    }
  }

  ngAfterViewChecked(){
    $('.menu .item').tab();
  }

  onRecipeCreated(recipe:Recipe){
  }

}
