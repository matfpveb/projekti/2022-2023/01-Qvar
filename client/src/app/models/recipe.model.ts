
export interface IRecipe{
    idMeal:string,
    strMeal:string,
    strCategory:string,
    strArea:string,
    strInstructions:string,
    strIngredients:[
        {
            name: String,
            measure: String
        }
    ],
    strMealThumb: string,
    strAuthor:string,
    strYoutube: string,
    rating:number
}
export class Recipe{
    constructor(
        public idMeal:string,
        public strMeal:string,
        public strCategory:string,
        public strArea:string,
        public strInstructions:string,
        public strIngredients:[
            {
                name: String,
                measure: String
            }
        ],
        public strMealThumb: string,
        public strAuthor:string,
        public strYoutube: string,
        public rating:number=0
    ){}
        
}