export interface IJwtTokenData {
    id: string,
    username: string,
    email: string,
    name: string,
    surname: string,
    imgUrl: string
}
