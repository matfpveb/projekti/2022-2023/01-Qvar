import { Recipe, IRecipe } from "./recipe.model";

export interface IRecipePagination {
    docs: IRecipe[],
    totalDocs: number,
    limit: number,
    totalPages: number,
    page: number,
    pagingCounter: number,
    hasPrevPage: boolean,
    hasNextPage: boolean,
    prevPage?: number,
    nextPage?: number
};
export class RecipePagination {
    docs: Recipe[] = [];
    totalDocs: number = 0;
    limit: number = 0;
    totalPages: number = 0;
    page: number = 0;
    pagingCounter: number = 0;
    hasPrevPage: boolean = false;
    hasNextPage: boolean = false;
    prevPage?: number;
    nextPage?: number;
}



